﻿using Microsoft.VisualStudio
using AnalyticsDeserializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyticsDeserializer.tests
{
    [TestClass()]
    public class AnalyticsDeserializerTests
    {
        // Sum != 16
        public struct CrashProtocolV1
        {
            [BitfieldLength(5)]
            public uint crashtype;
            [BitfieldLength(5)]
            public uint fileindex;
            [BitfieldLength(10)]
            public uint linenumber;
        };

        public struct CrashProtocolV2
        {
            [BitfieldLength(0)]
            public uint crashtype;
            [BitfieldLength(0)]
            public uint fileindex;
            [BitfieldLength(0)]
            public uint linenumber;
        };

        // Sum == 16
        public struct CrashProtocolV3
        {
            [BitfieldLength(3)]
            public uint crashtype;
            [BitfieldLength(3)]
            public uint fileindex;
            [BitfieldLength(10)]
            public uint linenumber;
        };

        CrashProtocolV1 v1 = new CrashProtocolV1();
        CrashProtocolV2 v2_sumDifferentThan16 = new CrashProtocolV2();
        CrashProtocolV3 v3_sum16 = new CrashProtocolV3();


        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void parseVersionNumberTest_argumentTest()
        {
            AnalyticsDeserializer.parseVersionNumber(new byte[] { 255,255,255 });
        }

        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        public void parseVersionNumberTest_argTestNullPtr()
        {
            AnalyticsDeserializer.parseVersionNumber(null);
        }

        [TestMethod()]
        public void parseVersionNumberTest_argTestValues()
        {
            // Iterates through values 0-255 assigned to each byte
            for (byte i = 0; i < 255; i++)
            {
                AnalyticsDeserializer.parseVersionNumber(new byte[] { i, i });
            }
            // Iterates through values 0-255 assigned to first, 255-0 to second
            for (int i = 0; i < 255; i++)
            {
                AnalyticsDeserializer.parseVersionNumber(new byte[] { (byte)i, (byte)(255 - i) });
            }
            // Iterates through values 255-0 to first, 0-255 to second
            for (int i = 0; i < 255; i++)
            {
                AnalyticsDeserializer.parseVersionNumber(new byte[] { (byte)(255 - i), (byte)i });
            }
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException), "The sum of given bitcounts must be 16")]
        public void parseCrashInfotest_sumDifferentThan16()
        {
            AnalyticsDeserializer.parseCrashInfo(ref v2_sumDifferentThan16, new byte[] { 255, 255 });
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException), "Bytearray length must be 2.")]
        public void parseCrashInfoTest_byteArrayLengthDifferentTest()
        {
            AnalyticsDeserializer.parseCrashInfo(ref v2_sumDifferentThan16, new byte[] { 255, 255, 64 });
        }

        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        public void parseCrashInfoTest_nullArgumentTest()
        {
            AnalyticsDeserializer.parseCrashInfo(ref v3_sum16, null);
        }

        [TestMethod()]
        public void parseCrashInfoTest_byteValueTest()
        {
            
            // Iterates through values 0-255 assigned to each byte
            for (byte i = 0; i < 255; i++)
            {
                AnalyticsDeserializer.parseCrashInfo(ref v3_sum16, new byte[] { i, i });
            }
            // Iterates through values 0-255 assigned to first, 255-0 to second
            for (int i = 0; i < 255; i++)
            {
                AnalyticsDeserializer.parseCrashInfo(ref v3_sum16, new byte[] { (byte)i, (byte)(255-i) });
            }
            // Iterates through values 255-0 to first, 0-255 to second
            for (int i = 0; i < 255; i++)
            {
                AnalyticsDeserializer.parseCrashInfo(ref v3_sum16, new byte[] {  (byte)(255 - i), (byte)i });
            }
        }
    }
}