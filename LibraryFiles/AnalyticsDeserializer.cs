﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyticsDeserializer
{
    [global::System.AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class BitfieldLengthAttribute : Attribute
    {
        uint length;

        public BitfieldLengthAttribute(uint length)
        {
            this.length = length;
        }

        public uint Length { get { return length; } }

    }

    public class AnalyticsDeserializer
    {
        public static Version parseVersionNumber(Byte[] aSerializedVersion)
        {
            try
            { 
                if(aSerializedVersion.Length != 2)
                {
                    throw new ArgumentException("aSerializedVersion length must be 2");
                }
                else
                {
                    // Append both bytes to represent a single 16-bit unsigned integer
                    int bitValue = aSerializedVersion[0] << 8;
                    bitValue = bitValue | aSerializedVersion[1];

                    // Do proper bitshifting and bitmasking
                    int major = bitValue >> 12;
                    int minor = (bitValue >> 6) & 0x3f;
                    int patch = bitValue & 0x3f;
                    return new Version(major, minor, patch);
                } 
            }
            catch (NullReferenceException)
            {
                throw new NullReferenceException("aSerializedVersion is null");
            }
        }

        public static CrashInfo parseCrashInfo<T>(ref T aCrashInfo, Byte[] aCrashInfoBytes)
        {
            if (aCrashInfoBytes != null || aCrashInfo != null)
            {
                if (aCrashInfoBytes.Length != 2)
                {
                    throw new ArgumentException("Bytearray length must be 2");    
                }
                else
                {
                    ushort[] bitIndexes = new ushort[3];
                    int i = 0;

                    foreach (System.Reflection.FieldInfo f in aCrashInfo.GetType().GetFields())
                    {
                        object[] attrs = f.GetCustomAttributes(typeof(BitfieldLengthAttribute), false);

                        if (attrs.Length == 1)
                        {
                            ushort fieldLength = (ushort)((BitfieldLengthAttribute)attrs[0]).Length;
                            bitIndexes[i] = fieldLength;
                            i++;
                        }
                    }

                    ushort reasonByteCount = bitIndexes[0];
                    ushort fileIndexByteCount = bitIndexes[1];
                    ushort lineByteCount = bitIndexes[2];
                    byte sum = (byte)(reasonByteCount + fileIndexByteCount + lineByteCount);

                    if (sum != 16)
                    {
                        throw new ArgumentException("The sum of given bitcounts must be 16.");
                    }

                    int bitValue = aCrashInfoBytes[0] << 8 | aCrashInfoBytes[1];
                    int remainingBits = 16;
                    ushort reason = (ushort)(bitValue >> remainingBits - reasonByteCount);
                    ushort bitMask = 0xffff;
                    remainingBits = remainingBits - reasonByteCount;
                    int shiftedMask = bitMask >> (ushort)(remainingBits - fileIndexByteCount) + reasonByteCount;
                    ushort fileIndex = (ushort)((bitValue >> remainingBits - fileIndexByteCount) & shiftedMask);
                    remainingBits = remainingBits - fileIndexByteCount;                  
                    ushort line = (ushort)(bitValue & Convert.ToInt32(Math.Pow(2, remainingBits) - 1));

                    return new CrashInfo(reason, fileIndex, line);
                }
            }
            else
            {
                throw new NullReferenceException();
            } 
        }
    }   
}