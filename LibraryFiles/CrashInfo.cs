﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyticsDeserializer
{
    public class CrashInfo
    {
        private ushort reason { get; set; }
        private ushort fileIndex { get; set; }
        private ushort line { get; set; }

        public CrashInfo(ushort reason, ushort fileIndex, ushort line)
        {
            this.reason = reason;
            this.fileIndex = fileIndex;
            this.line = line;
        }
        public override string ToString()
        {
            return "\nReason: " + this.reason + "\nFileIndex: " + this.fileIndex + "\nLine: " + this.line; 
        }
        public ushort getReason()
        {
            return this.reason;
        }

        public ushort getFileIndex()
        {
            return this.fileIndex;
        }

        public ushort getLine()
        {
            return this.line;
        }
    }
}
