﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnalyticsDeserializer
{
    static class Program
    {
        public struct CrashProtocolV1
        {
            [BitfieldLength(3)]
            public uint crashtype;
            [BitfieldLength(3)]
            public uint fileindex;
            [BitfieldLength(10)]
            public uint linenumber;
        };

        public struct CrashProtocolV2
        {
            [BitfieldLength(6)]
            public uint crashtype;
            [BitfieldLength(6)]
            public uint fileindex;
            [BitfieldLength(4)]
            public uint linenumber;
        };

        [STAThread]
        static void Main()
        {
          
            CrashProtocolV1 ver1 = new CrashProtocolV1();
            CrashProtocolV2 ver2 = new CrashProtocolV2();
            
            Version testVersion = AnalyticsDeserializer.parseVersionNumber(new byte[] { 16, 131 });
            Console.WriteLine(testVersion);
           
            CrashInfo testCrashInfo = AnalyticsDeserializer.parseCrashInfo<CrashProtocolV1>(ref ver1, new byte[] { 40, 3 });
            Console.WriteLine(testCrashInfo.ToString());
            
        }
    }
}
