# Build how-to  
#### Generating a solution file for Visual Studio 2017/2019  
- git clone git@bitbucket.org:oamkharjoittelu/analyticsdeserializerpackage.git  
- cd analyticsdeserializerpackage  
- mkdir build  
- cd build  
for Visual Studio 2019:  
- cmake -G "Visual Studio 16 2019" ../LibraryFiles    
for Visual Studio 2017:  
- cmake -G “Visual Studio 15 2017” ../LibraryFiles  

A .sln file is generated that includes main program that can be executed. Remember to set AnalyticsDeserializer as startup project (right click "AnalyticsDeserializer" in project solution -> set as StartUp project).    

#### Generating a NuGet package from library files  
- git clone git@bitbucket.org:oamkharjoittelu/analyticsdeserializerpackage.git  
- cd analyticsdeserializerpackage  
- cd LibraryFiles  
- nuget pack AnalyticsDeserializer.nuspec  

A .nupkg is generated that can be added to Visual Studio project or solution as a package.  

#### How to use NuGet package in Visual Studio project  
- Open your project file  
- Tools -> NuGet Package manager -> Manage Nuget Packages for Solution  
- Click the icon top right next to package source selection  
- Create a new package source and locate the AnalyticsDeserializer.nupkg file (should be in LibraryFiles by default)  
- Change the package source to newly created source -> Browse tab top left and install the package.  

#### Example usage  

 Version testVersion = AnalyticsDeserializer.AnalyticsDeserializer.parseVersionNumber(new byte[] { 255, 255 });  

